"use strict";

var parseUri = function(url) {

    var uri = function() {
        
        var indexStart = url.indexOf("?");

        return url.slice(indexStart);
    
        },
        isEmptyObj = function(obj) {
            return Object.keys(obj).length == 0 ? true : false
        },
        getUrl = function(obj) {

            var urlResult = url.slice(0, url.indexOf("?")),
                separator = "?";
            
            if (!obj) {
                return urlResult;
            }

            for (var key in obj) {
                urlResult = urlResult + separator + key + "=" + obj[key];
                separator = "&";
            }

            return urlResult
            
        };

    var UriProto = {
        getParams: function() {

            var result,
                paramsObj = {},
                search = /([^&=]+)=?([^&]*)/g,
                decode = function(str) {
                    return decodeURIComponent(str.replace(/\+/, " "));
                },
                string = uri().substring(1);

                while (result = search.exec(string)) {
                    paramsObj[decode(result[1])] = decode(result[2]);
                }

            return isEmptyObj(paramsObj) ? -1 : paramsObj;

        },
        setParams: function(obj, add) {

            var objDefault = this.getParams();

            if (!obj) {
                return getUrl({});
            }

            if (objDefault == -1) {
                return getUrl({})
            }

            if (add) {

                Object.assign(objDefault, obj);

                return getUrl(objDefault);   

            } 

            return getUrl(obj);

        }
    };

    return Object.create(UriProto);

};