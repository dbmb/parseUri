// Tests data

var URL = "stmobile://viewer/salesDoc?reopen=true&as=123",
    URL_EMPTY_PARAMS = "stmobile://viewer/salesDoc",
    obj = {
        reopen: "false",
        data: "yolo",
        idea: "3.41"
    };

// Tests
var test = {
    tests: [],
    describe: function(action, expect, func, mocks) {

        if (typeof action == "string") {
            action = {
                action: action,
                func:   func,
                expect: expect,
                mocks:  mocks
            };
        }
        this.tests.push(action);

    },
    run: function() {

        var result = "<html>";

        for (var backups = {}, checkResult, currentTest, testIndex = 0; currentTest = this.tests[testIndex++];) {

            if (currentTest.mocks) {
                for (var variable in currentTest.mocks) {
                    if (currentTest.mocks.hasOwnProperty(variable)) {
                        backups[variable] = window[variable];
                        window[variable] = currentTest.mocks[variable];
                    }
                }
            }

            checkResult = false;
            if (Array.isArray(currentTest.expect)) {
                checkResult = currentTest.expect.some(function fulfillsExpectations(expectation) {
                    return currentTest.func() === expectation;
                });
            } else {
                checkResult = currentTest.func() === currentTest.expect;
            }

            result +=
                "#" + testIndex + " " + currentTest.action + ": " +
                "<b><font color=\"" + (checkResult ? "#00FF00" : "#FF0000") + "\">" + checkResult + "</font></b><br>";

            if (currentTest.mocks) {
                for (var variable in currentTest.mocks) {
                    if (currentTest.mocks.hasOwnProperty(variable)) {
                        window[variable] = backups[variable];
                        backups = {};
                    }
                }
            }

        }

        result += "</html>";

        document.body.innerHTML = result;

    }

};

test.describe("Add params", "stmobile://viewer/salesDoc?reopen=false&as=123&data=yolo&idea=3.41", function() { return parseUri(URL).setParams(obj, true); });
test.describe("Set params", "stmobile://viewer/salesDoc?reopen=false&data=yolo&idea=3.41", function() { return parseUri(URL).setParams(obj); });
test.describe("Remove params", "stmobile://viewer/salesDoc", function() { return parseUri(URL).setParams(); });
test.describe("Get params", JSON.stringify({"reopen": "true", "as": "123"}), function() { return JSON.stringify(parseUri(URL).getParams()); });
test.describe("Get params empty", -1, function() { return parseUri(URL_EMPTY_PARAMS).getParams() });

test.run();